﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class questGeneration : MonoBehaviour
{

    public int itemTypeNumber;
    public int itemColorNumber;
    public int itemPatternNumber;

    public string itemType;
    public string itemColor;
    public string itemPattern;

    public string currentOrder;

    public string currentHexPass;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            GenerateItem();
        }
    }


    public void GenerateItem()
    {
        itemTypeNumber = Random.Range(1, 5);
        itemColorNumber = Random.Range(1, 7);
        itemPatternNumber = Random.Range(1, 3);

        //Type of item
        if (itemTypeNumber == 1)
        {
            itemType = "accesory";
        }
        if (itemTypeNumber == 2)
        {
            itemType = "tops";
        }
        if (itemTypeNumber == 3)
        {
            itemType = "bottoms";
        }
        if (itemTypeNumber == 4)
        {
            itemType = "dress";
        }
        if (itemTypeNumber == 5)
        {
            itemType = "outerwear";
        }



        if (itemColorNumber == 1)
        {
            itemColor = "red";
        }
        if (itemColorNumber == 2)
        {
            itemColor = "yellow";
        }
        if (itemColorNumber == 3)
        {
            itemColor = "green";
        }
        if (itemColorNumber == 4)
        {
            itemColor = "blue";
        }
        if (itemColorNumber == 5)
        {
            itemColor = "pink";
        }
        if (itemColorNumber == 6)
        {
            itemColor = "black";
        }
        if (itemColorNumber == 7)
        {
            itemColor = "white";
        }



        if (itemPatternNumber == 1)
        {
            itemPattern = "silk";
        }
        if (itemPatternNumber == 2)
        {
            itemPattern = "cotton";
        }
        if (itemPatternNumber == 3)
        {
            itemPattern = "polyester";
        }


        currentOrder = "You need to provide a " + itemColor + " " + itemType + " made out of " + itemPattern;
        
        currentHexPass = itemTypeNumber.ToString() + itemColorNumber.ToString() + itemPatternNumber.ToString();
        print("The item hex is " + currentHexPass);
    }

}
